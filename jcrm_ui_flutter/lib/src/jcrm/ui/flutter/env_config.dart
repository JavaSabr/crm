class EnvConfig {
  static const registrationUrl = String.fromEnvironment("registration_url");
  static const dictionaryUrl = String.fromEnvironment("dictionary_url");
  static const clientUrl = String.fromEnvironment("client_url");
}